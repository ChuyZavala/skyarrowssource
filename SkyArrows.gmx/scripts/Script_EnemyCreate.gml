startX=-100
floorY=650

switch(argument0)
{
//////////////////LV1
case 0://AO
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=AO
ins.Sprite_Atack=AO_Atack
ins.Sprite_Dead=AO_Dead
ins.Atack_Distance=700
ins.vel=3;
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;
break;

case 1://Calabera
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Shooter);
ins.Sprite_Walk=Calavera
ins.Sprite_Atack=Calavera_Atack
ins.Sprite_Dead=Calavera_Dead
ins.Atack_Distance=300
ins.Sprite_Bullet=Calavera_Bullet
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=0
ins.Shoot_OffsetY=-40
ins.vel=2
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 2://Chochinobake
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Chochinobake
ins.Sprite_Atack=Chochinobake_Atack
ins.Sprite_Dead=Chochinobake_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 3://Fantasma
break;

case 4://Karakasa
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Sprite_Walk=Karakasa_Jump
ins.Sprite_Jump=Karakasa_Jump
ins.Sprite_Atack=Karakasa
ins.Sprite_Dead=Karakasa_Dead
ins.Atack_Distance=700
ins.air[0]=false;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=false;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;ins.air[20]=true;ins.air[21]=true;ins.air[22]=true;ins.air[23]=true;ins.air[24]=true;ins.air[25]=true;ins.air[26]=true;ins.air[27]=true;ins.air[28]=true;ins.air[29]=true;ins.air[30]=true;ins.air[31]=true;ins.air[32]=true;ins.air[33]=true;ins.air[34]=true;ins.air[35]=true;ins.air[36]=true;ins.air[37]=true;ins.air[38]=true;ins.air[39]=true;ins.air[40]=true;ins.air[41]=false;ins.air[42]=false;ins.air[43]=false;ins.air[44]=false;ins.air[45]=false;ins.air[46]=false;ins.air[47]=false;ins.air[48]=false;ins.air[49]=false;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 5://Monkey
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Monkey
ins.Sprite_Jump=Monkey_Jump
ins.Sprite_Atack=Monkey_Atack
ins.Sprite_Dead=Monkey_Dead
ins.Atack_Distance=700
ins.air[0]=true;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=true;ins.air[10]=true;ins.air[11]=true;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 5.5://Monkey_Jump
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Sprite_Walk=Monkey
ins.Sprite_Jump=Monkey_Jump
ins.Sprite_Atack=Monkey_Atack
ins.Sprite_Dead=Monkey_Dead
ins.Atack_Distance=700
ins.air[0]=true;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=true;ins.air[10]=true;ins.air[11]=true;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

//////////////////LV2
case 6://BeHolder
break;

case 7://Jabali
var ins=instance_create(startX,floorY,Obj_Enemy_Embestida);
ins.Sprite_Walk=Jabali
ins.Sprite_Dead=Jabali_Dead
ins.Atack_Distance=1000
ins.vel=8
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;
break;

case 8://Mamut
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Mamut
ins.Sprite_Atack=Mamut_Atack
ins.Sprite_Dead=Mamut_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 9://IceMen
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Sprite_Walk=IceMen
ins.Sprite_Jump=IceMen
ins.Sprite_Atack=IceMen_Atack
ins.Sprite_Dead=IceMen_Dead
ins.Atack_Distance=700
ins.air[0]=true;ins.air[1]=true;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=true;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;ins.air[20]=true;ins.air[21]=true;ins.air[22]=false;ins.air[23]=false;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 10://Pinguin
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Pinguin
ins.Sprite_Atack=Pinguin_Atack
ins.Sprite_Dead=Pinguin_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 11://Jeti
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Sprite_Walk=Yeti_Jump
ins.Sprite_Jump=Yeti_Jump
ins.Sprite_Atack=Yeti_Atack
ins.Sprite_Dead=Yeti_Dead
ins.Atack_Distance=700
ins.air[0]=false;ins.air[1]=false;ins.air[2]=true;ins.air[3]=true;ins.air[4]=true;ins.air[5]=true;ins.air[6]=true;ins.air[7]=true;ins.air[8]=true;ins.air[9]=true;ins.air[10]=true;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=true;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 12://Wendigo
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Sprite_Walk=Wendigo
ins.Sprite_Jump=Wendigo
ins.Sprite_Atack=Wendigo_Atack
ins.Sprite_Dead=Wendigo_Dead
ins.Atack_Distance=700
ins.air[0]=true;ins.air[1]=true;ins.air[2]=true;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=6
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;
///////////////////////LV3
case 13://Armature
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Armature
ins.Sprite_Atack=Armature_Atack
ins.Sprite_Dead=Armature_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 14://Archeopterix
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Archeopterix
ins.Sprite_Atack=Archeopterix_Atack
ins.Sprite_Dead=Archeopterix_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 15://Dimetrodon
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Dimetrodon
ins.Sprite_Atack=Dimetrodon_Atack
ins.Sprite_Dead=Dimetrodon_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 16://Pteranodon
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Pteranodon
ins.Sprite_Atack=Pteranodon_Atack
ins.Sprite_Dead=Pteranodon_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 17://Raptor
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Raptor
ins.Sprite_Atack=Raptor_Atack
ins.Sprite_Dead=Raptor_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

case 18://Styracosarus
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Sprite_Walk=Styracosaurus
ins.Sprite_Atack=Styracosaurus_Atack
ins.Sprite_Dead=Styracosaurus_Dead
ins.Atack_Distance=700
ins.vel=3
ins.pow=1;
ins.pow2=1;
ins.bullet_power=1
ins.Hp=100;
ins.def=0;

break;

///////////////////////Lv4

}
